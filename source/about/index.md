---
layout: page
---

Podria dir que he creat aquest blog per satisfer la necessitat interior d'expressar-me, de comunicar al món el meu caos interior. Podria dir que tinc pensaments interessants a compartir, històries a narrar, viatges a explicar, però res d'això seria cert. El fet és que la major part de seccions d'aquest blog ja existien com a blogs independents sota diferents noms, en serveis de hosting gratuïts desperdigats per internet, però alguns d'aquests serveis han començat a fallar i he hagut de buscar una alternativa. Finalment, i veient que no podia pagar un hosting com Déu mana, he decidit utilitzar Gitlab Pages i per facilitar la migració he decidit amuntegar-ho tot aquí.

Així doncs, a les noves Cròniques hi trobareu les seccions següents:

- Memòries técniques: seguint l'exemple del blog de l'Aniol Martí, faré un apartat técnic on especifiqui els procediments per instal·lar, configurar o modificar qualsevol cosa, de manera que, tant un jo del futur com qualsevol altre persona, pugui reproduir els passos que he realitzat.
- Teoria del Caos: jo intentant entendre les matemàtiques i descobrint coses curioses. Vol ser una continuació del meu blog de matemàtiques, tot i que, amb una mica de sort, se m'escaparà alguna cosa de física, astronomia, electrònica o tecnologia en general...
- Metafísica de matinada: les Cròniques originals. Tots els posts d'aquesta categoria han estat escrits entre les 2 i les 5 de la matinada, en un estat mental dubtós i amb els ulls emboirats pel cansament.
- La biblioteca d'Alexandria: Aquest apartat pretendrà ser un recull de pel·lícules, música, llibres, contingut avdiovisual, gràfic... Potser una mera llista de coses interessants des del meu punt de vista, potser un recull detallat i ben classificat de coses qe he anat descobrint. En tot cas, no espereu trobar-hi coses massa corrents ni modernes.
