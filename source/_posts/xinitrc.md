---
title: xinitrc i xsession 
date: 2019-09-22 23:21:00
categories: Memòries tècniques 
---
Xinit és un programa que permet iniciar instàncies d'Xorg manualment que es crida internament quan fem **startx**.  A **startx** li podem passar opcionalment un fitxer de configuració indicant quins programes i ordres s'han d'executar, però per defecte utilitzarà el fitxer de configuració `~/.xinitrc` si existeix i `/etc/X11/xinit/xinitrc` en cas contrari. En aquest fitxer hi posarem totes les ordres que s'hagi d'executar al iniciar l'entorn gràfic. Per exemple, si  volgués executar un entorn gràfic amb **dwm** com a gestor de finestres, afeginr una barra d'estat definida per un script determinat i configurar el fons d'escriptori amb **feh** , l'arxiu `~/.xinitrc`  seria similar al següent:

```
#!/usr/bin/env bash
~/code/useful_files/dwm/status_dwm.sh &
~/.fehbg &
exec dwm
```

Si volem afegir un d'aquests entorns d'esciptori o gestors de finestres a un sistema  que ja té display manager, el més probable és que utilitzin l'arxiu `~/.xsession`. El funcionament és similar al de l'`~/.xinitrc`: un cop s'inicia sessió amb el display manager, s'executa el contingut d'aquest arxiu. En aquest cas, és bastant comú fer un link simbólic que apunti a `~/.xinitrc`:
```
ln -s ~/.xinitrc ~/.xsession
```
També hi ha Display Managers més moderns, que  permeten triar l'entorn d'escriptori abans d'iniciar sessió. Com s'inicien els entors dependrà del gestor que triem. A LightDM, per exemple, s'utilitzen fitxers `*.desktop` per a definir entors entorns d'escriptori. La versió més senzilla d'aquests arxius és la següent:

```
[Desktop Entry]
Name= Nom de la sessió
Exec= script_a_executar.sh
```
Guardant l'arxiu a `/usr/share/xsessions`, el proper cop que iniciem sessió, podrem seleccionar aquest entorn d'escriptori. Si volem que s'executi l' `~/.xsession` com esperaríem, s'haurà d'afegir una sessió amb el contingut següent:
```
[Desktop Entry]
Name=Xsession
Exec=/etc/X11/Xsession
```
Així es cridarà a `~/.xsession`, que en quest cas és un link simbòlic fa referència a `~/.xinitrc`, de manera que s'executarà el contingut d'aquest últim.
