---
title: Són temps difícils per als somiadors 
date: 2018-12-24 04:22:16
tags: cròniques
categories: Biblioteca d'Alexandria
---

> Sense tu, les emocions d'avui no serien més que la pell morta de les d'ahir.

Hipolito a _Le fabuleux destin d'Amélie Poulain_, probablement la millor pel·lícula que he vist mai.


