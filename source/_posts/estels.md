---
title: Estels al vent
date: 2018-05-04 03:36:54
tags: cròniques
categories: Metafísica de matinada 
---

A vegades penso que, amb els 19 anys que porto de vida, no he fet gaire res que valgui la pena; que porto massa temps preocupat pel passat i el futur, sense viure el present i, en definitiva, que he pres massa decisions equivocades.

A vegades penso que voldria ser un estel. Un estel al que se li ha trencat la corda i ara vola lliurement pel cel. Un estel que es perd entre els núvols i s'en va a veure món.
I d'entre els núvols, en una petita clariana, veuria la teva finestra, entreoberta. Vindria a buscar-te i, de sobte, hi hauria dos estels al cel. Dos estels que la quitxalla perseguiria pels carrers, tot rient i admirant-los. Dos estels que, custodiats pels ocells i ballant al so de la música del vent, volarien lluny, ben lluny... 

Dins el llibre hi ha un estel. És el primer d'uns quants que vaig fer, és el que ha tocat les estrelles. És el teu. Si vols, el pots alliberar d'immediat, però t'aconsello que el guardis. Dóna'l al vent quan sentis tot el pes del món sobre teu, deixa que s'emporti les tristeses i tensions, que es fonguin a l'aire. O ans al contrari, treu-li la corda en un moment de gran felicitat i fes que la propagui per allà on el vent el porti...

Que per molts anys puguis volar.

Gràcies per tot.
