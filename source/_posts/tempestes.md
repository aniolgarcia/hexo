---
title: Tempestes 
date: 2019-01-26 01:28:54
tags: cròniques
categories: Metafísica de matinada 
---


Fa fred, però la petita finestra de l'habitació és oberta. Hi he tret el cap per veure el carrer, ara buit, i mirant cap a la façana, he vist que totes les finestres excepte aquesta tenen les persianes baixades.

Torno a dins i, al cap d'una estona d'observar, m'adono que per la finestra hi entren quatre coses.

La primera és la llum. La llum prové, en part, d'un fanal que s'alça al cap del carrer. És un fanal majestuós i ornamentat però, com la majoria dels fanals, sap que això poc importa. La gent no para atenció als fanals i només els valora per la llum que fan. Per això la llum dels fanals és freda, sense ànima.  L'altra part de la llum prové de la Lluna que, amb timidesa, treu el cap per darrere de núvols i edificis. La seva llum és blanca com la neu, però, en canvi, és càlida com el foc.

El següent element és l'aire. L'aire, que ha entrat a l'habitació xiulant d'alegria, juga amb els fulls que hi ha sobre la taula i els fa volar per l'habitació. Després d'un curt vol i unes quantes piruetes, els fulls cauen lentament al terra, convertint-lo en un mar de paraules, nombres i figures.

El tercer element és, potser, el més subtil de tots. Un ha de parar prou atenció per adonar-se de la seva presència, tot i que poc a poc es va fent més evident. El tercer element és l'aigua. Les gotes entren silenciosament per la finestra i impacten contra la taula de fusta, ara nua. Hi ha gotes que cauen prop d'altres i utilitzen el quasi impecreptible pendent de la taula per tornar-se a ajuntar.  

L'últim element és el so: un dolç so de trons, encara llunyans, com tambors que acompanyen la rítmica col·lisió de les gotes amb la resta del món. S'aproxim

De la mateixa manera que n'entren quatre coses, per conservar el fràgil equilibri de l'univers, n'han de sortir quatre més.

La primera és la calor. Un petit radiador s'esforça per mantenir l'habitació a una temperatura raonable, però una bona part de la calor que desprèn surt fora. Per dins, rondina enfadat amb mi per estar per malgastar l'energia escalfant el  carrer en lloc de l'habitació, cosa que fa que sembli que emet encara més escalfor. Li explico que la ciutat té fred i que l'està ajudant a escalfar-se i sembla que es tranquilitza una mica.

La segona és una dolça olor que prové d'un got que hi ha a terra. Encara fumejant, una exòtica infusió preparada curosament desprèn una olor d'entre pebre i canyella, amb un toc de dent de lleó. Olors d'Índia que perfumen l'escena.

També en surt una imatge. És una imatge que no veu ningú però que la tinc ben gravada a la ment: una imatge d'una petita habitació a les fosques, amb un noi estirat a terra amb un estol de papers al voltant, mirant per una finestra oberta de bat a bat mentre es desencadena una tempesta.

L'última és una mirada. Una mirada que transmet incertesa, dubte i por, que busca auxili. Una mirada que busca uns altres ulls que el socorrin i, en no trobar-los, s'enfosqueix. Però en veure que encara al cel hi ha estrelles i en adonar-se que els núvols mai les podran apagar, transmet calma. I és que a vegades la calma no precedeix la tempesta, sinó que coexisteix amb ella. 


