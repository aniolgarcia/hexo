---
title: Ciutat Podrida 
date: 2017-08-11 00:38:57
tags: cròniques
categories: Metafísica de matinada 
---

Apago l'últim llum del carrer. El carrer s'adorm i es suma al silenci aparent de la ciutat. Esperen que quan s'il·lumini el cel una munió de formigues surtin del seu cau per córrer sense rumb. Mai no veuran el sol, doncs una espessa capa de contaminació, alimentada per les penes i tristors de cadascun dels habitants, s'encarrega de tapar tota esperança. El pulmó malalt i negre de la ciutat respira aquesta espessa boirina. S'ofega.

I arribarà el dia en què deixis de ser conscient d'aquesta realitat. Arribarà el dia en què circulis per l'extens formiguer de manera automàtica, absort en els teus pensaments i amb els ulls mig aclucats degut a la impossibilitat de descansar. Arribarà el dia en què pugis per escales mecàniques i ascensors, oblidant-te de que encara pots pujar a peu. Arribarà el dia en què correràs per agafar el bus tot i no tenir pressa, sense saber ben bé perquè. Arribarà el dia en que t'oblidaràs de caminar i et perdràs per sempre per les galeries del metro. Arribarà el dia en què no recordaràs que el món no necessàriament ha de ser d'aquesta manera. T'oblidaràs que darrera aquesta cortina de fum hi ha muntanyes, rius i platges: un món per explorar, aprendre i sentir, lluny de l'angoixa de la ciutat.

Quan arribi aquest dia, hauràs perdut. La teva ànima ja no serà teva, la ciutat s'haurà apoderat de tu.

Ciutat podrida, ens portes la nit i la por. Ara que ets adormida, els carrers són plens de foc.


