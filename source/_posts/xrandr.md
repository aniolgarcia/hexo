---
title: Afegint resolucions a  Xrandr 
date: 2018-11-10 03:04:53
tags: tècnic
categories: Memòries tècniques 
---
A vegades, en sistemes GNU/Linux, la informació de les resolucions dels monitors connectats no és del tot correcta, fent que no poguem seleccionar una resolució que sabem que el monitor suporta. A continuació presento una manera d'afegir manualment aquestes configuracions.

El primer que hem de fer és veure quin és el monitor al que hem d'afegir la configuració. Amb Xrandr obtindtem informació dels monitors connectats i de les resolucions i framerates detectades:
```
$ xrandr
Screen 0: minimum 320 x 200, current 1280 x 800, maximum 8192 x 8192
LVDS-1 connected primary 1280x800+0+0 (normal left inverted right x axis y axis) 261mm x 163mm
   1280x800      60.00*+  59.99    59.97    59.81    59.91    50.00  
   1280x720      60.00    59.99    59.86    59.74  
   1024x768      60.04    60.00  
   960x720       60.00  
VGA-1 connected (normal left inverted right x axis y axis)
   1024x768      60.00  
   800x600       60.32    56.25  
   848x480       60.00  
   640x480       59.94 
HDMI1 disconnected (normal left inverted right x axis y axis)
DP1 disconnected (normal left inverted right x axis y axis)
```
En el nostre cas, Xrandr detecta LVDS-1 (la pantalla del portàtil) i VGA-1 (l'externa), amb resolució màxima de 1024x768. Nosaltres volem afegir la resolució 1400x1050 al monitor VGA-1. Per fer-ho, haurem de crear un nou mode d'Xrandr i assignar-lo al monitor. El problema és que crear un nou mode no és una tasca fàcil, doncs si llegim el manual d'Xrandr veurem que necessitem una gran quantitat de paràmetres corresponents als valors de la modeline d'Xorg. I aquí és on entra en joc CVT. CVT és un porgrama que ens calcularà aquesta modeline en funció de la resolució. Tan sols cal executar cvt amb la resolució i, de manera opcional, el framerate:
```
$ cvt 1400 1050 60
# 1400x1050 59.98 Hz (CVT 1.47M3) hsync: 65.32 kHz; pclk: 121.75 MHz
Modeline "1400x1050_60.00"  121.75  1400 1488 1632 1864  1050 1053 1057 1089 -hsync +vsync
```
Ara l'hem d'afegir a Xrandr:
```
$ xrandr --newmode "1400x1050_60.00"  121.75  1400 1488 1632 1864  1050 1053 1057 1089 -hsync +vsync
```
I afegim el mode al monitor corrsponent:
```
$ xrandr --addmode VGA-1 "1400x1050_60.00"
```
I amb tot això, a la configuració del monitor de la vostra distribució ja us hauria d'aparèixer aquesta configuració. De tota manera, si voleu aplicar-la directament per la línia de comandes, tan sols cal fer

```
xrandr --output VGA-1 --mode "1400x1050_60.00"
```

ATENCIÓ: Teòricament això es mentindrà durant la sessió actual, però en sortir, es reviertirà la configuració. Per fer-ho persistent s'hauria de crear un fitxer .xprofile amb les tres últimes comandes al directori arrel de l'usuari.



