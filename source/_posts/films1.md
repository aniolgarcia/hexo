---
title: Pel·lícules que vull veure 
date: 2019-03-18 02:15:27 
tags: cròniques
categories: Biblioteca d'Alexandria 
---


Recentment he anat trobant diverses pel·lícules que voldria veure i, degut a la meva falta de temps, la llista creix molt més ràpid del que disminueix... Com que començava a ser massa gran per la meva memòria limitada, he decidit escriure-la i, ja que tinc aquesta secció del blog, publicar-la aquí. La llista no segueix cap mena d'ordre ni organització però, si continua creixent, em plantejaré classificar les pel·lícules.

- Chico y Rita
- Cinema Paradiso
- The Bycicle Thief
- Captain Fantastic
- La Bella y la Bestia (sí, sí, ja ho sé...)
- Les Amants du Pont-Neuf
- Some Kind of Wonderful
- Vivir es Fácil con los Ojos Cerrados
- Bande à Part
- The Dreamers
- Into the Wild
- Katmandú, un Espejo en el Cielo
- Stargate: The Ark of Truth
- Lost in Translation
- Out of Africa
- March Comes in Like a Lion
- Medianeras
- As I Was Moving Ahead Occasionally I Saw Brief Glimpses of Beauty
- Children Who Chase Lost Voices
- Sing Street
- Castaway on the Moon
- Spirited Away
- Pieta in the Toilet
- Taipei Story

