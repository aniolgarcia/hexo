---
title: Chungking Express 
date: 2018-12-19 01:37:12
tags: cròniques
categories: Biblioteca d'Alexandria
---

Avui estava molt enfadat, quasi furiós. Enfadat fins al punt de tenir ganes de colpejar i destruir qualsevol objecte que s'em posés al davant, cosa poc usual en mi. M'he assegut en un banc per respirar profundament i mirar d'aclarir-me el cap i he decidit destinar tota aquesta energia destructiva a fer el camí fins al lloc on visc corrents. Tota aquesta situació m'ha fet recordar una una frase d'una pel·lícula

> We're all unlucky sometimes. When I am, I go jogging. The body loses water when you jog, so you have none left for tears.

que traduït vindria a dir

> Tots tenim mala sort, a vegades. Quan em passa, vaig a còrrer. El cos perd aigua quan corres, de manera que no en queda per a les llàgrimes.

Us puc assegurar que no és veritat.

Però bé, parlem de la pel·lícula, que és el que ens interessa. Es tracta de "Chungking Express", la primera pel·lícula de Wong Kar-Wai que vaig veure. "Chungking Express", ambientada a Hong Kong, ens planteja dues històries amb dos policies com a protagonistes. La primera ens parala del policia 223, i com viu el fet que la seva parella de feia 5 anys el deixi. La segona parla del policia 663, que també l'ha deixat la seva xicota hostessa de vol. Tots dos policies tenen maneres peculiars d'assumir i tractar la situació i veiem com tots dos coneixen a personatges també peculiars.

La pel·lícula sap transemtre el frenesí que desprén Hong Kong i el contrast que fa entre la ciutat sempre activa i plena de gom a gom amb la solitud i aïllament dels personatges es simplement fantàstic. 

Si us agrada Quentin Tarantino, recomano veure [aquest vídeo](https://www.youtube.com/watch?v=RoHg-RvcwzE) després d'haver vist la pel·lícula. Tarantino explica quatre coses del metratge, els actors, l'obra de Wong Kar-Wai i quines pel·licules mirar si us ha agradat aquesta.

I per acabar, un ["muntatge musical"](https://www.youtube.com/watch?v=KSpeKBhXhwg&t=18s) amb seqüències de la pel·licula que captura bastant la seva escència.



