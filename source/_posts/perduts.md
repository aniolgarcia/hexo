---
title: Perduts 
date: 2018-04-22 01:24:29
tags: cròniques
categories: Metafísica de matinada 
---

Tots som ànimes perdudes, intentant trobar el nostre lloc en el món... I jo pregunto, no podem imaginar-nos que el món no existeix, tan sols aquesta nit?

I de sobte, la fina línia del temps s’esquerda. I quedem suspesos en un tendre buit, a l’espera de que tot s’acabi. La melodia de la nostra respiració ens porta cada vegada més amunt. Lluny... hem d’anar més lluny... Lluny de patiments, de tristeses i de dolors. Lluny de l’angoixa que portem dins, la que constantment ens recorda la seva presència esquinçant-nos l’ànima en mil bocins.

Poc a poc ens hi apropem, ens acostem al cel infinit. I de cop, tot s’apaga. Tot menys nosaltres. Brillem amb la llum de mil sols, amb la il·lusió del primer vol d’un ocell. Durant un curt instant sentim que tenim tota l’energia del cosmos, tota la vida de l’univers ens pertany...

Però és tan sols un curt moment. Un moment en què recordem que existim, recordem qui som i qui vàrem ser... La foscor s’apropa a nosaltres, tot apagant-nos. I poc a poc perdem l’energia i tornem a les tenebres... Tornem a ser esclaus del temps... Ànimes perdudes intentant trobar el nostre lloc...

