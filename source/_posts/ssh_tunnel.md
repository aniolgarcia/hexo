---
title: Redirecció de ports mitjançant túnels SSH
date: 2018-11-27 10:07:23
categories: Memòries tècniques 
---
Recentment m'he trobat en la situació d'haver d'accedir a un port que no accepta connexions remotes d'un servidor qualsevol. Si es té accés al router, una opció seria modificar-ne la configuració, obrir un port extern i fer la redirecció de ports corresponent, exposant així el port a connexions externes. El cas és que jo no tenia accés al router i, si n'hagués tingut, no m'hauria fet massa gràcia exposar el servei de manera oberta, així que vaig haver de buscar una alternativa. Remenant per la xarxa, vaig descobrir que SSH és capaç de gestionar la redirecció de ports i resulta que jo tenia accés a la màquina per SSH, de manera que ja tenia una solució.

Per al meu problema, l'únic que va ser necessari va ser fer una redirecció local de ports però, llegint amb més calma el manual d'SSH, m'he adonat que tot això és molt més potent i útil del que inicialemnt m'havia imaginat.

## Redirecció local
Útil en un escenari com el meu: un port d'un servidor al que només es pot accedir des de `localhost`.  Suposem que es tracta del port `9000` del servidor `exemple.cat`, i que el volem redirigir al port `1234` del nostre ordinador.
```
$ ssh -L 1234:localhost:9000 usuari@exemple.cat
```
Amb aquesta comanda estrm redirigint el port `9000` del servidor `exemple.cat` al port `1234` del nostre ordinador, de manera que ja hi tenim accés. __ALERTA: aquí localhost no fa referència al nostre ordinador, sinó a localhost des del punt de vista del servidor!__ De fet, executant aquestà línia, ens connectarem al servidor i aquest ens donarà una línia de comandes. Si només volem fer la redirecció de ports i no ens cal la línia de comandes del servidor, podem fer
```
$ ssh -nNT -L 1234:localhost:9000 usuari@exemple.cat
```
Evidentment, si el port d'SSH del servidor no és el `22` sinó que és el `2200`, per exemple, escriurem
```
$ ssh -p 2200 -L 1234:localhost:9000 usuari@exemple.cat
```

La reidrecció local també ens ofereix altres possiblilitats. Per exemple: suposem que estem en una xarxa que ens bloqueja l'accés a `web.cat`, però no les connexions SSH. Podem fer
```
$ ssh -L 1234:web.cat:80 usuari@exemple.cat
```

Aquesta comanda redirigirà el port `80` (el port per defecte d'HTTP) de `web.cat` al port `1234` del nostre ordinador a través del servidor `exemple.cat`.

## Redirecció remota
La redirecció remota és equivalent a la redirecció local però des de la banda del servidor. Ara en lloc de "demanar" accés al port d'un servidor des d'una màquina remota, donarem accés des del servidor a aquesta màquina. La sintaxi és la següent:
```
$ ssh -R 1000:localhost:3000 usuari@exemple.cat
```

Amb aquesta comanda estem dient que el port `1000` del servidor `exemple.cat` "escolti" el port `3000` de la nostra màquina local.
