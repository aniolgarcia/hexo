---
title: Hello World!
date: 2017-07-03 01:24:29
tags: cròniques
categories: Metafísica de matinada 
---

Benvolgut desconegut,

Potser t'estàs preguntant com has acabat a aquí. Jo també m'ho pregunto, la veritat. Què t'ha portat a fer click a l'enllaç que apunta a aquesta pàgina? I què carai faig jo aquí escrivint? No tinc resposta per a cap de les dues preguntes.

Potser aquestes accions han estat espontànies, mer producte de la casualitat, però d'alguna manera o altre tu segueixes aquí llegint i jo segueixo escrivint, i això m'agrada. I ara els dos sentim curiositat. Tu et preguntes qui és la persona que escriu. Serà algú que s'avorreix? Un boig? Algú que necessita ser escoltat i no pot parlar? Que necessita expressar el seu caos interior? Probablement només serà un boig, conclous. I jo podria preguntar el mateix del lector, però no ho faré. Fonamentalment perquè pot ser que tu no existeixis i això no ho llegeixi mai ningú, o pot ser que tan sols ho llegeixi jo mateix, i en aquest últim cas ja en conec la resposta.

Potser tu ja te n'has cansat, però jo encara sento curiositat. Vull explorar les llibertats d'escriure sense la necessitat d'expressar un missatge útil, i potser fins i tot sense la necessitat d'un receptor.

Així doncs, siguis qui siguis, fins i tot encara que no existeixis, només em queda dir-te una cosa: benvingut al meu món.

